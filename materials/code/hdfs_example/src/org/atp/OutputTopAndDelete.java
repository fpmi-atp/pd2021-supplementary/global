package org.atp;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OutputTopAndDelete {

    public static void main(String[] args) throws IOException {
        Path outPath = new Path(args[0]); // путь - аргумент командной строки
        FileSystem fs = FileSystem.get(new Configuration()); // Configuration - свойства файловой системы
        int top = 5; // хотим вывести 5 строк
        if(!fs.exists(outPath)){  // есть ли файл?
            System.err.println("Incorrect path!");
            return;
        }
        outTop(fs, outPath, top);
        fs.deleteOnExit(outPath);
    }

    /**
     * Вывести ТОП-N строк в директори HDFS. Полезно в том случае если нужно вывести ТОП по отсортированным данным.
     */
    private static void outTop(FileSystem fs, Path path, int top) throws IOException {
        int linesCount = 0;
        for (FileStatus status: fs.listStatus(path)){ //FileStatus описывает файл (или директорию) в HDFS (путь, время создания, права...)
            BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status.getPath())));
            String line = null;
            // читаем построчно пока не наберется топ
            while ((line = br.readLine()) != null && linesCount < top){
                System.out.println(line);
                linesCount++;
            }
        }
    }
}
